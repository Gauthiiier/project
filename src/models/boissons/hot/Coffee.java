package models.boissons.hot;

public class Coffee extends HotBoisson {

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public double cost() {
        return 50.5;
    }

    @Override
    public String getDescription() {
        return "Coffee";
    }

    @Override
    public void brew() {
        System.out.println("Pour coffee");

    }

    @Override
    public void addCondiment() {
        System.out.println("Add milk");
    }

    @Override
    public HotBoisson clone() {
        return new Coffee() ;
    }

}
