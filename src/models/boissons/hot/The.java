package models.boissons.hot;

public class The extends HotBoisson{

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public double cost() {
        return 20.5;
    }

    @Override
    public String getDescription() {
        return "Tea";
    }

    @Override
    public void brew() {
        System.out.println("Pour tea");
    }

    @Override
    public void addCondiment() {
        System.out.println("Add lemon");
    }

    @Override
    public HotBoisson clone() {
        return new The() ;
    }

}
